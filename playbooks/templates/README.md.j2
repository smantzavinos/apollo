# {{ arc['space']['name'] }}

This is the auto-generated README for [apollo](https://gitlab.com/p3r.one/apollo) space {{ arc['space']['name'] }}.

## Details

- Container-Engine: {{ arc['engine']['provider'] }}
- Container-Orchestrator: {{ arc['orchestrator']['provider'] }}
- Data-Provider: {{ arc['data']['provider'] }}
- Base Domain: {{ arc['space']['base_domain'] }}
- Space Domain: {{ arc['space']['space_domain'] }}

## Infrastructure

- Infrastructure-Provider: {{ arc['infrastructure']['provider'] }}

### Nodes

{% for host in groups['cluster'] %}
- {{ host }}: {{ hostvars[host]['ansible_default_ipv4']['address'] }}
{% endfor %}

## Data

{% if arc['data']['provider'] == "nfs" %}
apollo runs with NFS as data provider. This means:

- manager-0 exports {{ arc['data']['volumes_dir'] }} to all workers to the same directory via NFS
{% if arc['addons']['gitlab-runner']['deploy']['enabled'] %}
- manager-0 exports `$APOLLO_VOLUMES_DIR={{ arc['data']['volumes_dir'] }}` to the `deploy` gitlab-runner's environment for you to consume it from within your pipelines.
{% endif %}
- {{ arc['data']['volumes_dir'] }} should be used to create volume-directories and bind-mount them into services
- volume-directories need to be created first; this can be done by using this command: `docker run --rm -v ${APOLLO_VOLUMES_DIR}:${APOLLO_VOLUMES_DIR} alpine:latest mkdir -p ${APOLLO_VOLUMES_DIR}/service-volume`
{%endif%}
{% if arc['federation']['enabled']|bool %}

## Federation

- Enabled: True
- Federation-User: {{ apollo_federation_user }}
- Federation-Password: {{ apollo_federation_password }}
- Federation-Spaces:
{% for space in apollo_federation_spaces %}
  - {{ space }}
{% endfor %}
{% endif %}

## Backups

By default, backups of {{ arc['addons']['backup']['source'] }} will be saved to {{ arc['addons']['backup']['repository'] }} every 30 minutes.

{% if arc['addons']['backup']['enabled']|bool %}
- Enabled: True
- Provider: {{ arc['addons']['backup']['provider'] }}
- Source: {{ arc['addons']['backup']['source'] }}
- Storage-User: {{ arc['addons']['backup']['aws_access_key_id'] }}
- Storage-Password: {{ arc['addons']['backup']['aws_secret_access_key'] }}
- Storage (Repository): {{ arc['addons']['backup']['repository'] }}
- Backup-Keyphrase: {{ arc['addons']['backup']['password'] }}
{% else %}
- Enabled: False
{% endif %}

To access the backups from a remote source, use the above credentials to connect to the Storage Repository. To access the backups from `manager-0`, simply run `restic`. All necessary configuration on the manager node has been done in advance.

To restore backups, please refer to restic's [documentation](https://restic.readthedocs.io/en/latest/).

## Authentication

apollo uses **Basic Authentication** to authenticate access on controlplane services:

- Username: {{ arc['auth']['admin_user'] }}
- Password: {{ arc['auth']['admin_password'] }}

## DNS Setup

- Base Domain: {{ arc['space']['base_domain'] }}
- Space Domain: {{ arc['space']['space_domain'] }}
- Ingress IP: {{ apollo_ingress_ip }}
- Management IP: {{ apollo_management_ip }}

By default, apollo expects ingress traffic to land on the worker nodes which is why the Ingress IP defaults to `worker-0`'s IP.

To operate this space, please add the following records to your DNS zone (`{{ arc['space']['base_domain'] }}`):

```bash
{{ arc['space']['name'] }} IN A {{ apollo_management_ip }}
*.{{ arc['space']['name'] }} IN A {{ apollo_management_ip }}
```

This gives you access to the controlplane services (see below).

To point a domain (e.g. {{ arc['space']['base_domain'] }}) to the cluster, use Ingress IP:

```bash
{{ arc['space']['base_domain'] }} IN A {{ apollo_ingress_ip }}
*.{{ arc['space']['base_domain'] }} IN A {{ apollo_ingress_ip }}
```

## Deployment

To deploy this space, run the following commands from this space directory:

### if you use a cloud provider and did not yet spin up your infrastructure

```bash
apollo build
apollo deploy
```

### if you use your own nodes and already prepared a Nodesfile.yml

```bash
apollo deploy
```

## How to enable automated versioning for this space

By default, this space includes [shipmate](https://gitlab.com/p3r.one/shipmate) pipelines that handle automatic versioning and more.

Please refer to shipmate's [README](https://gitlab.com/p3r.one/shipmate) regarding necessary requirements for this to work with your GitLab repository (Personal Access Token).

## Access

### SSH

A set of SSH-Keys belonging to this space can be found in `.ssh/`. The keys can be used to connect as user **root** to any of the IPs in `APOLLO_NODES_MANAGER` or `APOLLO_NODES_WORKER`:

{% for host in groups['cluster'] %}
- {{ host }}: {{ hostvars[host]['ansible_default_ipv4']['address'] }}
{% endfor %}

Assuming you cloned the repository and work from its local directory, you can connect via ssh like this:

`ssh -l root -i .ssh/id_rsa {{ arc['space']['space_domain'] }}`

Once connected, you can reach any other node in the cluster by simply doing `ssh worker-0` etc.

From within your space directory, you can also do this:

```bash
# replace manager-0 with any of the nodes you intend to enter
apollo enter manager-0
```

### VS Code Remote Development

Read up on [VS Code Remote Development](https://code.visualstudio.com/docs/remote/remote-overview) first.

- Clone this repo
- cd into the repository directory
- add your ssh-key to the apollo manager: `ssh-copy-id -i ~/.ssh/id_rsa -f -o "IdentityFile .ssh/id_rsa" root@{{ arc['space']['space_domain'] }}`
- Setup [SSH](https://code.visualstudio.com/docs/remote/ssh-tutorial)
- Click the "Open a remote window" button, select "Remote SSH: connect to host", then "+ Add new SSH Host"
- Enter `ssh root@{{ arc['space']['space_domain'] }}`
- Enjoy

### Web (Controlplane)

You can reach any of the controlplane services through their webinterface. Use `{{ arc['auth']['admin_user'] }}` and `{{ arc['auth']['admin_password'] }}` to authenticate.

- [Analytics](https://grafana.{{ arc['space']['space_domain'] }}:8443): Grafana dashboards for the cluster
- [Traefik](https://traefik.{{ arc['space']['space_domain'] }}:8443): Controlplane proxy dashboard
- [Alerts](https://alerts.{{ arc['space']['space_domain'] }}:8443): Alerts dashboard

### Web (Engine / Orchestrator)

You can reach any of the orchestrator-related services through their webinterface. Use `{{ arc['auth']['admin_user'] }}` and `{{ arc['auth']['admin_password'] }}` to authenticate.

- [Portainer](http://portainer.{{ arc['space']['space_domain'] }}): Docker/Swarm management interface
- [Proxy](http://proxy.{{ arc['space']['space_domain'] }}): Engine proxy dashboard

## How to deploy apps to this space

Ideally you deploy with [shipmate](https://gitlab.com/p3r.one/shipmate). Ansible can be used to automate a few things around volumes and networks but you can also use `docker stack deploy` directly.

The default mechanism (requires `arc.addons.gitlab-runner.deploy.enabled: true`) makes use of the special gitlab-runner with the `deploy` tag that has manager-0's docker socket mounted. Thus you'll be able to interface with the cluster's manager from within a shipmate container directly and can use standard tooling. 

Also, you can deploy using the following techniques:

- setup ssh to use this space's ssh-key for connections, then set `DOCKER_HOST=ssh://$SPACE_DOMAIN` when using `docker stack deploy`
- ssh to manager-0 and deploy stuff locally (using `apollo enter manager-0`)
- use the space with VSCode Remote Development which also gives you direct access to the Docker Socket